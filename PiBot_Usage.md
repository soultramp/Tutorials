# Guide zur Benutzung des PiBots

## Vorbemerkungen
Der PiBot, hier speziell der Raspberry Pi als Herzstück, wird ab jetzt nur noch per Fernzugriff (`SSH`) gesteuert. Das bedeutet, wir müssen weder Bildschirm noch Maus und Tastatur anschließen.

## Verbindung per SSH
Um euch nun mit eurem Raspi zu verbinden, schaltet ihr diesen ein und wartet bis die Startsequenz abgelaufen ist und eine IP auf dem Display des Roboters angezeigt wird.  
Nun öffnet auf dem Laptop ein neues Terminal Fenster und gebt dort folgenden Befehl ein:
```
$ ssh pi@10.0.0.<Eure Zahl>
```
Anstelle hier `<Eure Zahl>` müsst ihr die Zahl eingeben, die euch auf dem Display ausgegeben wird.  
Danach werdet ihr nach einem Passwort gefragt. Gebt dafür das euch zugewiesene Passwort ein. Ihr könnt das Passwort auch nachträglich noch über den Konsolenbefehl `passwd` ändern.

Nun solltet ihr mit dem Raspi verbunden sein und könnt Konsolenbefehle auf ihm ausführen.

## Einführung in die Programmierung
Um in die Programmierung in Python einzusteigen, haben wir diesmal das Format etwas umgestellt. Wir haben für euch sogennante Jupyter Notebooks vorbereitet. Das sind interaktive Tutorials, die auf dem Raspi in einem kleinen Webserver laufen. So könnt ihr auf die Notebooks von eurem Rechner aus zugreifen. Vorher müssen wir aber erstmal den Webserver starten. Das könnt ihr wie folgt machen:
```
$ jupyter notebook --ip <Die IP eures Pi's>
```
Anschließend sollte es bei euch ähnlich wie hier aussehen:
![](./assets/jupyter.png)
Wenn ihr nun (Mit `Ctrl` gedrück) auf den unteren Link klickt, dann sollte sich in eurem Browser eine Webseite öffnen. Diese zeigt die Ordnerstruktur des Homeverzeichnisses eures Raspis. Navigiert nun zu `RoboSchool -> Tutorials` und klickt dort auf das File namens `Python_Tutorial.ipynb`. Arbeitet als nächstes dieses Tutorial durch und meldet euch einfach, falls es Fragen gibt.

## Weiterführende Programmierung
Sobald ihr mit dem `PiBot_Programming_Guide.ipynb` durch seid, könnt ihr euch nun entscheiden, wie ihr fortfahrt. Ihr könnt entweder die kommenden Aufgaben, die euch auf den Wettbewerb vorbereiten, weiterhin in Jupyter Notebooks lösen (Erstellt dafür euer eigenes Notebook unter `~/RoboSchool`). Diese Methode ist zwar für den Wettbewerb ungeeignet, jedoch lassen sich gewisse Ideen damit äußerst schnell testen.

Die zweite, bevorzugte, Methode ist, dass ihr euch auf dem Laptop ein Projekt mit der IDE names `PyCharm` erstellt und dieses dann immer auf den Raspi synchronisiert. Im folgenden wird gezeigt, wie ihr euch ein solches Projekt einrichtet:

1. Öffnet das Startmenü und gebt dort `PyCharm` ein. Wählt dort unbedingt **PyCharm Professional Edition** aus.
2. Geht nun auf `Create New Project` und wählt dort unter `Location` den gewünschen Pfad zu eurem Projekt aus
3. Klickt dann auf `Project Interpreter: New Virtual environment` und stellt das rote Feld wie unten beschrieben ein. Das weiße könnt ihr anpassen, wie ihr wollt
  ![](./assets/pycharm_create_project.png)
4. Danach klickt auf `Create` und das Projekt wird erstellt.
5. Mit einem Rechtsklick auf den Ordner, der den Namen hat wie ihr euer Projekt genannt habt, könnt ihr ein neues Python File erstellen in dem ihr euren Python Code schreibt:
  ![](./assets/pycharm_create_file.png)
6. Um euren Code auf den Raspi zu schieben, bietet uns PyCharm das sogenannte Deployment Feature and, welches uns ermöglicht, dass sobald wir Code in dem Projekt ändern und abspeichern, PyCharm automatisch diesen Code per SFTP auf den Raspi schickt. Diese Funktion müssen wir aber zunächst noch einrichten. Geht dafür auf `Tools -> Deployment -> Configuration...`
7. Clickt danach auf das `+` Symbol, wählt `SFTP` aus und gebt eurem Raspi einen Namen.
8. In der Konfiguration, füllt alles wie in dem folgenden Screenshot aus, das einzige was ihr anpassen müsst, ist die IP Adresse.
  ![](./assets/deployment_config.png)
9. Im Tab `Mappings` könnt ihr nun einstellen, wohin euer Projekt auf den Raspi geschoben werden soll. Dabei wird der Pfad relativ zum Home Directory (`home/pi`) aufgelöst. Beispiel folgt (Ihr müsst hier nicht den Namen `test` verwenden. Benutzt möglichst einen sinnvollen Namen, den ihr gut erkennen könnt):
  ![](./assets/deployment_mapping.png)
10. Geht nun in der Toolbox zu `Tools -> Deployment -> Options...` und fügt in dem ersten Feld `;venv` hinten an das bereits geschriebene an. Zusätzlich macht noch den Haken bei `Create empty directories` und `Delete target items...` rein.
11. Als letztes müsst ihr nur noch auf `Tools -> Deployment -> Automatic Upload` drücken und dann müsste euer geschriebener Code automatisch beim speichern direkt auf den Raspi geschoben werden.
12. Nun könnt ihr per `SSH` euern Python Code auf dem PiBot starten. Öffnet dafür euer Terminal, in welchem ihr per SSH mit dem PiBot verbunden seid und führt euer File mit `python` aus:
    ```python
    $ python <name_des_files>.py
    ``` 